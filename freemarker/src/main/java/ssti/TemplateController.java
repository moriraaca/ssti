package ssti;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TemplateController {

    @Autowired
    private Configuration configuration;

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public String hello(Map<String, Object> model, @CookieValue(required = false, name = "username") String b64username, @CookieValue(required = false, name = "template") String b64template) throws IOException {
        String decodedUsername = null != b64username ? new String(Base64.getDecoder().decode(b64username)) : "";
        model.put("username", decodedUsername);

        if (null != b64template) {
            String decodedTemplate = new String(Base64.getDecoder().decode(b64template));
            model.put("template", decodedTemplate);
            try {
                Writer userTemplateOut = new StringWriter();
                Template template = new Template("userTemplate", decodedTemplate, configuration);
                Map<String, Object> templateModel = new HashMap<>();
                templateModel.put("username", decodedUsername);
                template.process(templateModel, userTemplateOut);
                model.put("emailMessage", userTemplateOut.toString());
            } catch (ParseException | TemplateException e) {
                model.put("error", "Error in template: " + e.getMessage());
            }
        }

        return "hello";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/updateUsername")
    public String updateUsername(@RequestParam("username") String username, HttpServletResponse response) {
        response.addCookie(new Cookie("username", Base64.getEncoder().encodeToString(username.getBytes())));
        return "redirect:/";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/updateEmailMessage")
    public String updateEmailMessage(@RequestParam("template") String template, HttpServletResponse response) {
        response.addCookie(new Cookie("template", Base64.getEncoder().encodeToString(template.getBytes())));
        return "redirect:/";
    }

}