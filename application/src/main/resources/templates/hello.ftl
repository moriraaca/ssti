<!DOCTYPE html>

<html lang="en">

<body>
	<h1>Hello dear user: <#outputformat "HTML">${username!"Anonymous"}</#outputformat></h1>
	<#if error??><span style="display: block; padding-bottom: 15px; color: red"><#outputformat "HTML">${error}</#outputformat></span></#if>
	<form action="/updateUsername" method="POST">
	    <label for="username" style="display: block; padding-bottom: 10px">Change your username:</label>
	    <input type="text" name="username" style="display: block; padding-top: 5px; width: 400px" value="<#outputformat "HTML">${username!""}</#outputformat>"/>
	    <input type="submit" style="display: block; margin-top: 10px"/>
	</form>
</body>

</html>