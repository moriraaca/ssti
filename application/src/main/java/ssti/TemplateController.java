package ssti;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;
import java.util.Map;

@Controller
public class TemplateController {

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public String hello(Map<String, Object> model, @CookieValue(required = false, name = "username") String b64username) throws IOException {
        if (null != b64username) {
            String decodedUsername = new String(Base64.getDecoder().decode(b64username));
            model.put("username", decodedUsername);
        }

        return "hello";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/updateUsername")
    public String updateUsername(@RequestParam("username") String username, HttpServletResponse response) {
        response.addCookie(new Cookie("username", Base64.getEncoder().encodeToString(username.getBytes())));
        return "redirect:/";
    }

}