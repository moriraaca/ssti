# README #

**BE EXTREMELY CAREFUL! RUNNING THIS CODE MAY EXPOSE YOUR MACHINE TO EXPLOITATION! WHEN RUNNING SERVERS, MAKE SURE THEY ARE ONLY ACCESSIBLE VIA LOCALHOST, AND IDEALLY - TURN OFF INTERNET CONNECTION COMPLETELY!**

## Usage ##


### Java ###
All Java applications located in directories [application](application), [freemarker](freemarker), [freemarker-blind](freemarker-blind), [freemarker-identification-examples](freemarker-identification-examples) and [velocity](velocity) are written using Spring Boot. You can start respective servers using gradle, by running following command from main directory:
```bash
./gradlew <application-directory>:bootRun
```

For example:

```bash
./gradlew application:bootRun
```

Will run [application](application) example.

### Python ###
All Python applications located in directories [jinja2](jinja2) and [jinja2-sandboxed](jinja2-sandboxed) are written using Flask. You can start respective servers by running following command from main directory:

```bash
python <application-directory>/application.py
```

For example:

```bash
python jinja2/application.py
```

Will run [jinja2](jinja2) example.

Python applications might require additional modules to be installed.