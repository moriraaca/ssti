<!DOCTYPE html>

<html lang="en">

<body>
    <a href="/">Back</a>
    <hr/>
	<form action="/sendTemplate" method="POST">
        <label for="template" style="display: block;">Here is your template:</label>
        <textarea rows="10" cols="66" name="template" style="display: block; margin: 10px"><#outputformat "HTML">${template!""}</#outputformat></textarea>
        <input type="hidden" name="redirect" value="${redirect}"/>
        <input type="submit" style="display: block; margin: 10px"/>
    </form>
	<span style="display: block;">And here is result:<span>
	<#if error??>
	    <span style="display: block; padding: 10px; color: red"><#outputformat "HTML">${error}</#outputformat></span>
	<#else>
	    <textarea rows="10" cols="66" name="template" style="display: block; margin: 10px" disabled="disabled"><#outputformat "HTML">${result!""}</#outputformat></textarea>
	</#if>

</body>

</html>