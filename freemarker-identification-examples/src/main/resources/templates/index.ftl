<!DOCTYPE html>

<html lang="en">

<body>
    <ul>
	    <li><a href="/fullTemplate">Full template context</a></li>
	    <li><a href="/textContext">Text context</a></li>
	    <li><a href="/executableContext">Executable context</a></li>
	    <li><a href="/timeBasedBlind">Time-based blind</a></li>
	</ul>
</body>

</html>