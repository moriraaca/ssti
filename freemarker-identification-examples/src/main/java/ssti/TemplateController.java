package ssti;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TemplateController {

    @Autowired
    private Configuration configuration;

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public String index() {
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/fullTemplate")
    public String showFullTemplate(Map<String, Object> model, @CookieValue(required = false, name = "template") String b64template) throws IOException {
        if (null != b64template) {
            String decodedTemplate = new String(Base64.getDecoder().decode(b64template));
            model.put("template", decodedTemplate);
            try {
                Writer userTemplateOut = new StringWriter();
                Template template = new Template("userTemplate", decodedTemplate, configuration);
                Map<String, Object> templateModel = new HashMap<>();
                templateModel.put("username", "someusername");
                template.process(templateModel, userTemplateOut);
                model.put("result", userTemplateOut.toString());
            } catch (ParseException | TemplateException e) {
                model.put("error", "Error in template: " + e.getMessage());
            }
        }

        model.put("redirect", "fullTemplate");

        return "template";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/textContext")
    public String showTextContext(Map<String, Object> model, @CookieValue(required = false, name = "template") String b64template) throws IOException {
        if (null != b64template) {
            String decodedTemplate = new String(Base64.getDecoder().decode(b64template));
            model.put("template", decodedTemplate);
            try {
                Writer userTemplateOut = new StringWriter();
                Template template = new Template("userTemplate", "Hello ${username}, your input is here: " + decodedTemplate, configuration);
                Map<String, Object> templateModel = new HashMap<>();
                templateModel.put("username", "someusername");
                template.process(templateModel, userTemplateOut);
                model.put("result", userTemplateOut.toString());
            } catch (ParseException | TemplateException e) {
                model.put("error", "Error in template: " + e.getMessage());
            }
        }

        model.put("redirect", "textContext");

        return "template";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/executableContext")
    public String showExecutableContext(Map<String, Object> model, @CookieValue(required = false, name = "template") String b64template) throws IOException {
        if (null != b64template) {
            String decodedTemplate = new String(Base64.getDecoder().decode(b64template));
            model.put("template", decodedTemplate);
            try {
                Writer userTemplateOut = new StringWriter();
                Template template = new Template("userTemplate", "Hello ${username?" + decodedTemplate + "}, how are you doing?", configuration);
                Map<String, Object> templateModel = new HashMap<>();
                templateModel.put("username", "someusername");
                template.process(templateModel, userTemplateOut);
                model.put("result", userTemplateOut.toString());
            } catch (ParseException | TemplateException e) {
                model.put("error", "Error in template: " + e.getMessage());
            }
        }

        model.put("redirect", "executableContext");

        return "template";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/timeBasedBlind")
    public String showTimeBasedBlind(Map<String, Object> model, @CookieValue(required = false, name = "template") String b64template) throws IOException {
        if (null != b64template) {
            String decodedTemplate = new String(Base64.getDecoder().decode(b64template));
            model.put("template", decodedTemplate);
            try {
                Writer userTemplateOut = new StringWriter();
                Template template = new Template("userTemplate", decodedTemplate, configuration);
                Map<String, Object> templateModel = new HashMap<>();
                templateModel.put("username", "someusername");
                template.process(templateModel, userTemplateOut);
                model.put("error", "I won't show you result");
            } catch (ParseException | TemplateException e) {
                model.put("error", "Error in template: " + e.getMessage());
            }
        }

        model.put("redirect", "timeBasedBlind");

        return "template";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/sendTemplate")
    public String sendTemplate(@RequestParam("template") String username, @RequestParam("redirect") String redirect, HttpServletResponse response) {
        response.addCookie(new Cookie("template", Base64.getEncoder().encodeToString(username.getBytes())));
        return "redirect:/" + redirect;
    }

}